mediafragmenter estas ilo por farado de fragmentaj indicoj.
Fragmenta indico estas dosiero kun priskribo pri dosiera tipo, vojo, starto kaj
fino de la fragmento.
Ĉi tiu versio de la pakaĵo laboras kun fragmentoj de videaĵoj, aŭdioj kaj
simplaj tekstaj dokumentoj (txt, xml, source code kaj tiel plu).

Startigo de fragmentoj estas atingebla per alia programo:
fragplayer --help

Uzado:

mediafragmenter <dosiero> [-t|--type <tipo>] [-s|--start <starto>] \
                          [-f|--finish <fino>] [-o|--output <eliga dosiero>]
    Nur vojo al dosiero estas bezonata, aliaj parametroj ne estas nepraj.
    
    Per flago "-t" ("--type") oni povas indiki dosieran tipon: "video", "audio",
    "text". Se la flago ne estas uzata, la tipo determiniĝas aŭtomate.
    
    Flagoj "-s" ("--start") kaj "-f" ("--finish") determinas limoj de fragmento:
    
        por aŭdioj kaj videoj estas uzataj tempaj markoj en aranĝo HH:MM:SS
        (horoj, minutoj, sekundoj kun dupunktoj);
        sekundoj povas havi neentjeronon (ne plu ol kvar signoj post punkto);
        
        por tekstoj, limo estas numero de linio kaj numero de signo de la linio,
        dividitaj per komo (sen blanko); limoj estas fermitaj, tio estas
        fragmentoj inkluzivigas ĉi tiuj limojn;
        
        malesto de starta limo signifas starto de fragmento en starto
        de dosiero; malesto de fina limo signifas fino de fragmento en fino
        de dosiero;
    
    Flago "-o" ("--output") permesas determini vojon/nomon de eligita
    fragmenta indiko. Defaŭlte, la indiko ekhavos nomon de originala dosiero
    kun finaĵo ".fragpointer".
    
    Ekzemploj:
    
    mediafragmenter ~/Koncerto.mkv -s 01:43:14 -f 01:47:38 -o kanto.fragpointer
    Indiko al videa fragmento estiĝos kun nomo "kanto.fragpointer".
    
    mediafragmenter ~/Notoj.txt -s 45,1 -f 59,31
    Fragmenta indiko estos kreata kun nomo "Notoj.fragpointer", ĝi havos
    limoj de 45a linio al 31a signo de 59a linio (inkluzive).
    
    mediafragmenter ~/cartoon.avi -f 00:00:59 -o intro.fragpointer
    Ĉi tiu komando kreos indikon al videaĵo de ĝia starto al 59asekunda marko.    

mediafragmenter --version
    Montras la version de la programo.

mediafragmenter --help
    Montras ĉi tiun helpon.

© Eugene 'Vindex' Stulin, 2019
