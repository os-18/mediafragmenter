/*
 * Copyright (C) 2019 Eugene 'Vindex' Stulin
 *
 * This file is a part of mediafragmenter and fragplayer.
 * mediafragmenter and fragplayer is free software:
 * you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import std.algorithm, std.array, std.string, std.regex;
import amalthea.langlocal;

immutable packageVersion = import("version").strip;


enum MediaType {
    audio,
    video,
    text
}

immutable string errmsg;
shared static this() {
    initLocalization(translations);
    errmsg = "Invalid format of the fragment pointer."._s;
}

string fragPointerPreprocessing(string content) {
    string[] lines = content.split("\n").array;
    string modifiedContent;
    foreach(line; lines) {
        if (line.startsWith("#")) continue;
        if (line.strip.empty) continue;
        modifiedContent ~= line.split("#")[0] ~ "\n";
    }
    modifiedContent = modifiedContent.stripRight('\n');
    lines = modifiedContent.split("\n").array;
    bool firstLineIsType = lines[0].startsWith("Type: ");
    bool secondLineIsPath = lines[1].startsWith("Path: ");
    if (!firstLineIsType || !secondLineIsPath) {
        throw new InvalidFragPointer(errmsg);
    }
    return modifiedContent;
}


/**
 * The function of checking expressions for "start" and "finish" of fragments
 */
void checkPointerValidity(MediaType mediaType, string pointer) {
    final switch(mediaType) {
        case MediaType.video:
            string hh = "[0-9]?[0-9]?[0-9]?[0-9]?[0-9]";
            string mm = "[0-9][0-9]";
            string ss = "[0-9][0-9]";
            string fraction = r"\.?[0-9]?[0-9]?[0-9]?[0-9]?";
            auto hhmmss = "^" ~ hh ~ ":" ~ mm ~ ":" ~ ss ~ fraction ~ "$";
            auto hhmm = "^" ~ hh ~ ":" ~ mm ~ "$";
            if (pointer != matchFirst(pointer, regex(hhmmss))[0]
             && pointer != matchFirst(pointer, regex(hhmm))[0]) {
                throw new InvalidFragPointer(errmsg);
            }
            break;
        case MediaType.audio:
            goto case MediaType.video;
        case MediaType.text:
            auto r = regex(r"^[1-9]+[0-9]*\,[ ]*[1-9]+[0-9]*$");
            if (pointer != matchFirst(pointer, r)[0]) {
                throw new InvalidFragPointer(errmsg);
            }         
            break;
    }
}

void showVersion() {
    writeln(packageVersion);
}


class InvalidFragPointer : Exception {
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}


immutable translations = [
    ["en_US", "ru_RU", "eo"],
    [
        "Invalid format of the fragment pointer.",
        "Неправильный формат указателя на фрагмент.",
        "Neregula datenaranĝo de fragmenta indico."
    ]
];
